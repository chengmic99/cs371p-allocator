// --------------
// RunAllocator.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Allocator.h"

// ----
// main
// ----

int main () {
    using namespace  std;

    run(cin, cout);
    return 0;
}