#include <iostream>
#include <cassert>
#include "Allocator.h"
using namespace std;

void run(istream& inStream, ostream& outStream) {
    int num_tests;
    inStream >> num_tests;
    assert(num_tests > 0 && num_tests <= 100);
//get rid of the initial leftover newlines
    inStream.get();
    inStream.get();
    while(num_tests-- > 0)
    {
        my_allocator<double,1000> a;
        int num;
        while (inStream.peek() != '\n' && inStream.peek() != EOF)
        {
            inStream >> num;
            if(num >= 0)
            {
                a.allocate(num);
            } else {
                auto myIter = begin(a);
                int i = 0;
                while (i > num)
                {
                    if (*myIter < 0)
                    {
                        --i;
                    }
                    if (i != num) {
                        ++myIter;
                    }
                }
                int* ptr = &(*myIter) + 1;
                a.deallocate(reinterpret_cast<double*>(ptr),0);
            }
//skip the new line
            inStream.get();
        }
        inStream.get();
        auto printer = begin(a);
        outStream << *printer;
        ++printer;
        while(&(*printer) < &a[999])
        {
            outStream << " " << *printer;
            ++printer;
        }
        outStream << endl;
    }
}