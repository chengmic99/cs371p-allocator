# CS371p: Object-Oriented Programming Allocator Repo

* Name: Michelle Cheng, Billy Vo

* EID: mc63834, bv5433

* GitLab ID: chengmic99, billyvo

* HackerRank ID: chengmic99, billyvo

* Git SHA: 79565e0046a0623d8be05895b7bfbc0e35d0ec65

* GitLab Pipelines: https://gitlab.com/chengmic99/cs371p-allocator/pipelines

* Estimated completion time: 12

* Actual completion time: 14

* Comments: Grade based on Michelle's GitLab Repo and HackerRank
