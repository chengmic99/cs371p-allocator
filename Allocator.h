// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <iostream>  // Allocator.c++ function declarations

using namespace std;
// ---------
// Allocator
// ---------


template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * <your documentation>
     */
    // bool valid () const {
    //     // <your code>
    //     // <use iterators>
    //     // return true;
    //     const iterator itr(reinterpret_cast<int*>(a));
    //     const iterator end(reinterpret_cast<int*>(a+N));
    //     while(itr != end) {
    //         int* sentinel2 = reinterpret_cast<int*>(reinterpret_cast<char*>(&*itr) + abs(*itr) + 4);
    //         if(*itr != *sentinel2) {
    //             return false;
    //         }
    //         ++itr;
    //     }
    //     return true;
    // }

    bool valid () const {
        int index = 0;
        while(index < N) {
            int sizeInStart = (*this)[index];
            index += std::abs(sizeInStart)+4;
            if(index >= N) {
                // std::cout << "******** sentinel val made us go above N" << std::endl;
                return false;
            }
            int sizeInEnd = (*this)[index];
            if(sizeInStart != sizeInEnd) {
                // std::cout << "******** sentinel vals don't match" << std::endl;
                return false;
            }
            index += 4;
        }
        return true;
    }

    // bool printheap () const {
    //     int index = 0;
    //     while(index < N) {
    //         int sizeInStart = (*this)[index];
    //         std::cout << "*********  sent1: " << sizeInStart << ", ";
    //         index += std::abs(sizeInStart)+4;

    //         int sizeInEnd = (*this)[index];
    //         std::cout << "sent2: " << sizeInEnd << std::endl;

    //         index += 4;
    //     }
    //     return true;
    // }

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            // <your code>
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            // <your code>
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            // <your code>
            int sentinel_value = abs(*_p);
            _p = reinterpret_cast<int*>(reinterpret_cast<char*>(_p) + 8 + sentinel_value);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            // <your code>
            int sentinel_value = abs(*(_p-1));
            _p = reinterpret_cast<int*>(reinterpret_cast<char*>(_p) - 8 - sentinel_value);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            // <your code>
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            // <your code>
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            // <your code>
            int sentinel_value = abs(*_p);
            _p = reinterpret_cast<int*>(reinterpret_cast<char*>(_p) + 8 + sentinel_value);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            // <your code>
            int sentinel_value = abs(*(_p-1));
            _p = reinterpret_cast<int*>(reinterpret_cast<char*>(_p) - 8 - sentinel_value);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        // int* start = reinterpret_cast<int*>(a);
        // start[0] = (N-8); // replace!
        // // <your code>
        // start+=1;
        // start+=((N-8)/4);
        // start[0] = (N-8);
        // std::cout<< " heusfhakshdja esuhfaowhe N  is " << N << std::endl;
        (*this)[0] = N - 8;
        (*this)[N-4] = N - 8;

        // printheap();
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type n) {
        // <your code>
        int desired_sentinel_val = n * sizeof(T);
        int* start = reinterpret_cast<int*>(a);

        while(start <= reinterpret_cast<int*>(a+N)) {
            int orig_sentinel_val = *start;
            if(orig_sentinel_val >= desired_sentinel_val) {
                pointer res = reinterpret_cast<pointer>((start)+1);
                // perfect malloc
                if(orig_sentinel_val == desired_sentinel_val ||
                        orig_sentinel_val - desired_sentinel_val - 4 < sizeof(T) + 4) {
                    *start = -abs(orig_sentinel_val);
                    start += 1;
                    start = reinterpret_cast<int*>(reinterpret_cast<char*>(start) + orig_sentinel_val);
                    *start = -abs(orig_sentinel_val);
                    // std::cout<<"perfect alloc " << n*sizeof(T) << " bytes" << std::endl;
                    // printheap();
                    assert(valid());
                    return res;
                }
                // split malloc
                else {
                    int right_block_sentinel_val = orig_sentinel_val - desired_sentinel_val - 8;
                    *start = -abs(desired_sentinel_val);
                    start+=1;
                    start = reinterpret_cast<int*>(reinterpret_cast<char*>(start) + desired_sentinel_val);
                    *start = -abs(desired_sentinel_val);
                    start+=1;
                    *start = abs(right_block_sentinel_val);
                    start+=1;
                    start = reinterpret_cast<int*>(reinterpret_cast<char*>(start) + right_block_sentinel_val);
                    *start = abs(right_block_sentinel_val);
                    // std::cout<<"split alloc " << n*sizeof(T) << " bytes" << std::endl;
                    // printheap();
                    assert(valid());
                    return res;
                }
            }
            start+=1;
            start = reinterpret_cast<int*>(reinterpret_cast<char*>(start) + abs(orig_sentinel_val));
            start+=1;
        }
        assert(valid());
        return nullptr;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * <your documentation>
     */
    void deallocate (pointer p, size_type n) {
        // <your code>
        int* ptr = reinterpret_cast<int*>(p) - 1;

        // TODO::::: check if p is in bounds

        int* curr_sentinel1 = ptr;
        int* curr_sentinel2 = reinterpret_cast<int*>(reinterpret_cast<char*>(ptr) + 4 + abs(*curr_sentinel1));
        int* left_sentinel2 = curr_sentinel1-1;
        int* right_sentinel1 = curr_sentinel2 + 1;

        int curr_sentinel_val = *curr_sentinel1;
        int left_sentinel_val = *left_sentinel2;
        int right_sentinel_val = *right_sentinel1;

        int* left_sentinel1 = reinterpret_cast<int*>(reinterpret_cast<char*>(left_sentinel2) - left_sentinel_val - 4);
        int* right_sentinel2 = reinterpret_cast<int*>(reinterpret_cast<char*>(right_sentinel1) + 4 + right_sentinel_val);


        //TODO:::::::: check if all L and R sentinels are even in bounds
        // merge both
        if(right_sentinel_val > 0 && right_sentinel2 < reinterpret_cast<int*>(a+N) && left_sentinel_val > 0 && left_sentinel1 >= reinterpret_cast<int*>(a)) {
            left_sentinel1[0] = 16 + abs(curr_sentinel_val) + left_sentinel_val + right_sentinel_val;
            right_sentinel2[0] = 16 + abs(curr_sentinel_val) + left_sentinel_val + right_sentinel_val;
            // std::cout<<"deallocate: merge both" << std::endl;
            // printheap();
            assert(valid());
            return;
        }
        // merge right
        if(right_sentinel_val > 0 && right_sentinel2 < reinterpret_cast<int*>(a+N)) {
            curr_sentinel1[0] = 8 + abs(curr_sentinel_val) + right_sentinel_val;
            right_sentinel2[0] = 8 + abs(curr_sentinel_val) + right_sentinel_val;
            // std::cout<<"deallocate: merge right" << std::endl;
            // printheap();
            assert(valid());
            return;
        }
        // merge left
        if(left_sentinel_val > 0 && left_sentinel1 >= reinterpret_cast<int*>(a)) {
            left_sentinel1[0] = 8 + left_sentinel_val + abs(curr_sentinel_val);
            curr_sentinel2[0] = 8 + left_sentinel_val + abs(curr_sentinel_val);
            // std::cout<<"deallocate: merge left" << std::endl;
            // printheap();
            assert(valid());
            return;
        }
        curr_sentinel1[0] = abs(curr_sentinel_val);
        curr_sentinel2[0] = abs(curr_sentinel_val);
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(reinterpret_cast<int*>(&a[0]));
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return iterator(reinterpret_cast<int*>(&a[0]));
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(reinterpret_cast<int*>(&a[N-1]));
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return iterator(reinterpret_cast<int*>(&a[N-1]));
    }


};
void run(istream & inStream, ostream & outStream);


#endif // Allocator_h